# Архитектура

Приложение использует паттерн микро-серверной архитектуры, когда множество сервисов общаются друг с другом посредством сообщений передаваемых через очереди.

В продакшене деплой должен происходить в kubernetes кластер. 

Во время локальной разработки можно использовать docker-compose для эмуляции системы, но без авто-масштабирования. Или minikube для тестирования авто-масштабирования.

Сообщения от и для сервисов будут находиться в очередях специфичных для каждого аккаунта. То есть для каждого аккаунта будут созданы несколько очередей. Точнее 2 очереди для каждого сервиса * на количество аккаунтов.

Для очередей будут использоваться высоко производительные очереди kafka или redis. Или и то и другое в зависимости от области применения. И kafka и redis могут работать с сотнями тысяч очередей.
- Для общения между сервисами внутри системы лучше использовать kafka так как она позволяет держать большее количество не прочитанных сообщений, которые могут накапливаться во время неактивности во время ожидания working windows.
- Для общения между сервисами внутри "блока" лучше использовать redis так как эта очередь работает быстрее и вообще редис легче поднять и контролировать.

---

# Модели данных
## Tenant / Account (customer facing)

Биллинг осуществляется ну этом уровне. 
- Для Тенантов выделяются отдельные ресурсы, таким образом мы можем гарантировать SLA.
- Обычные Аккаунты работают на общем железе, SLA там будет совсем другой. Для контроля SLA нужно будет наращивать мощности и полагаться на авто-масштабирования подов в kubernetes.

У одного аккаунта может быть несколько проектов.

## Project (customer facing)

На уровне Проекта прописываются
- Цели для сканирования
- Периодичность проверок. CRON расписание. Строка.
- Группы проверок
- Рабочие окна. Список CRON пар, начало и конец работы сервисов для каждого конкретного аккаунта.
- Конфигурация доступа в VPN. Для сканирования ресурсов во внутренней сети клиента.

---

# SLA (Service Level Agreement)

Мы можем гарантировать определенный SLA при выполнении проверок. В первую очередь связанные со скоростью проведения проверок и нагрузкой которую мы будем возлагать на сканируемый ресурс.

## Примеры SLA
- Количество сканируемых портов в единицу времени
- Количество security проверок в единицу времени
- Количество запросов к сканируемому ресурсу

SLA зависит как от программной части, так и от железной - мощность вычислительных ресурсов, ширина пропускного канала.

---

# Компоненты системы

## Сервис (docker - container, kubernetes - pod)
Контейнер, в котором запущена одна программа имеющий унифицированный интерфейс. Может быть написан как на питоне, так и на другом языке.

Сервис это долгоживущий процесс, он запускается один раз для обработки неограниченного количества задач.

Так как большинство задач будет связанно с общением по сети то большинство сервисов сможет одновременно асинхронно обрабатывать несколько задач. 

- Не имеет доступа к датабазе системы. Но может иметь доступ к собстенной датабазе.
- Может принимать задания из очереди или аргументов запуска.
- Выдает сообщения в очередь или консоль.
- Один "сервис" запускается в одном Kubernetes Pod. Kubernetes контролирует загрузку пода и при необходимости поднимает дополнительные поды.
- Может авто-масштабироваться горизонтально.
- В описании сервиса описывается схема входящего и исходящего сообщения. Используется для определения если 2 сервиса совместимы. Описание схемы сообщений используеться для серелиализации/десерелаизации сообщений.

## Зависимость (docker - container, kubernetes - pod)
По идее тот же сервис, но с доступом к ДБ системы. Можно сказать "системный сервис".

## Блок (docker - container, kubernetes - pod)
Помогает оптимизировать работу нескольких сервисов, контролирует RPS. Позволяет запускать несколько сервисов в одном процессе - создать "супер-сервис" для умещения количества kubernetes Pods. Например, несколько похожих "проверок" можно объединить в один блок.

Описание взаимодействий нескольких (или одного) "сервисов" или "блоков"
- Описание работы блока в формате YAML или JSON
- Блок контролирует/гарантирует min/max RPS, объем трафика.
- Сервисы внутри блока связаны через очереди с уникальными именами. TBD

### Пример конфигурации Сервиса

```yaml
- service_name: "masscan_service"  # Имя сервиса
- service_type: "unit"  # Тип сервиса - unit или block. У блока есть дополнительное поле graph. 
- service_settings: {}  # Настройки сервиса специфичные для каждого сервиса
- dependencies: []  # Указывает другие сервисы как зависимости
- input:
  - queue_type: "redis"  # тип очереди для входящий сообщений redis или kafka
  - queue_name: "masscan_jobs"  # имя очереди для входящий сообщений
  - queue_key: null
- output:
  - queue_type: "redis"  # тип очереди для исходящий сообщений redis или kafka
  - queue_name: "open_ports_from_masscan"  # имя очереди для исходящий сообщений
  - queue_key: null
- run_in_block: false  # должен ли сервис быть запущен как часть другого сервиса, а не в отдельном процессе
- desired_rps: 1000  # желаемая пропускная способность сервиса
- resources:  # Параметры для Kubernetes Pod. Влияют на производительность сервиса и авто-масштабирование.
  - limits:
    - cpu: 1
    - memory: 100Mb
  - requests:
    - cpu: 1
    - memory: 50Mb
- replica: 1
```

---

# Workflow v1

- Запускаем все сервисы и блоки.
- Находим все "зависимости" и запускаем их.

Все сервисы запущенны все время, но обрабатывают сообщения только во время working windows для каждого конкретного аккаунта. 

Остановка и запуск обработки сообщений во время Working Windows.
- Отдельный сервис мониторит начало и конец каждого "рабочего окна" для каждого аккаунта.
  - Когда окно начинается сообщение "event:window_start account_id:xxx" отправляется в очередь "system_events". Все запущенные сервисы подписываются на очереди с указанными account_id. 
  - Когда окно заканчивается сообщение "event:window_stop account_id:xxx" отправляется в очередь "system_events". Все запущенные сервисы отписываются от очередей с указанными account_id. 

# Workflow v2

Предполагает остановку kubernetes Pods для сервисов которые не используются ни в одном working window - для освобождения ресурсов.

---

# Диаграммы

## Workflow v1 - Пример работы сервисов и зависимостей
![image](https://mermaid.ink/svg/pako:eNrNVc1u00AQfpXRnlqpidS0vViCQ-mBCyDR3urIcu0tCcTrENvQqqrUpgeEWlKBkOAGvEGaJiRN2_QVZt-I2XVMssZwQByIZGU9--183-z8-IB5oc-ZxUqlki3ietzgFuAXeYQjvMGBPAJadvECJ7ItT0EeKxv2cESrCfYAR4BDAqQWdWYij2UbB3hlC-3zWctt1mBr3RZAvyjZSQ34qeDUqAz4WfN2ZBuwn5rlCd4BjmFjnbY_yFOydzVAa5jghXxLfLfk6xzwDidaMu3dae9dvdOBB0-fPC5DqkL9fN7kwufC23eWtwM3ijxXOM_DnahaiKlsk4wxeRtqj7ckYwBRrQbyHb32FS9ekZluqdjBSpGDmDcEj3_vgw7bIl2-THjCneUF_R9Z84od1_PCRMTO3t7eYob_edMRb72qe9zZaYTeC4rVZvhtlkUrJIVOM2zFkfO6HtecDC7cgMMCvsdrUjVetNlcWBlmedtwNdVUAKyYQBG4zfxdZ9CVHNQNmtUsJoMbSqX7M4ICTgOw8suFZjurJqEufiptbaJ8gTyR55S0nlndsKVTVzV9rf2dr83Nh1Uzz5Usz39KT2He5-saSmXK9ke8Vp2hau-76im8pB6hl7EyUVHeKmGqlYaqs0hrm4SdK1OPNNLJiXxDMvvqkWegun6s5gE9RxRQRyHHVCZd2j6lM7o5Mxe5BrVZmZIyrWWjsFOtc7cHxDoiFq0IFB3Fcaaop4oIqFTRBFIRzGlSnaWbqT-bASlxrhfypTA1w_99b5WCVFOxG4D88MltG6WWRvuVqLoU4kiJo_UNzALMzdhLFRPMV-K9dJCZl7z6z1miqGZSrNmCLbGAtwK37tNX7EBR2iyu8YDbzKKlz3fdpEHKbHFIUDeJw8194TErbiV8iSVN3435Rt2lWRkwa9dtRGTlfj0OW4_SL6P-QB7-AG8OoNA)

## Работа Сервиса (блок) "Открытые порты с указаникм сервиса"
![image](https://mermaid.ink/svg/pako:eNqdVM9u0zAYfxXLp1VqoOkFFE09IK6c4IYny0ncxShxQuIwoWlSux04bOLCHcEbbBMVU9myV7DfiM8JGU2WiY5UkRx_v3-yv36HOEhDjj3sOA6RSqiYe0h_0-f6QlfmWJ8j_V2vzEJf6iuzhM8dKPzSlV6PEMH6K0DWZmFOzbE51Sukb4G1sB_ILJE50WtQ-gnvjb6C9TXsbqoRTGRtPI_TgyBiuUJvXhCJ4ClKfz9nWYT0FxA8AeFbG2YN1jdmac7MZ31dS1X6smGUUijK3u76s83Iu0_9mZewogiY3GuAXIZEbnAC6g6yZMIy-i71i70W_p-hfOruPGQw6uCm_8LV2e3ifclLTlWaiYCymlWZTzVvpX-YM8sjgIT7qSAkHL6XZlzSLM3V6L6C_zgFeiBURAuefxABp5IlfEAz2Erz7pBHnTthjiMyz50-ezKBn4usq_d8MnGBDgXUVqb9yn3K1HFmndNqjTqbCFB37dBvDygSLDKaM7nP_5o3Hn92bYSxtatz9KHTPpRga9g5r6FcwaOcrWbbcVtrDUdrG7JzFCDbaPST1Dy02RAeUjyWXPXu62F4UUS1cactu3-O7b0jpbIhNTzGCc8TJkIYeYdWnWAV8YQT7MEy5HNWxsqOpSOAslKlrz_KAHsqL_kYl1nIFH8pGAyBBHtzFhewy0Oh0vxVM0braXr0GwIJJQ4)

## Структура Сервиса (Generic)
![image](https://mermaid.ink/svg/pako:eNqlUsFOAjEQ_ZVNj4T9gT144qgXOVJDmnaARrZLurMmhpCghMSbX-DBP0AiyUKUb5j-kQO7rGK82R76OvPeazudqdCZAZGIOI6lQ4tjSCJ6DY9hHha043UR5rTiEG0YrKkMD7SS7kgfejUZRZfX0kU8rOvnegSp6rF-SRv6OOjWDPf0Hp55LcMyYvme51t4YsYnlbSV0imts8Jh35okytFzpJVErZvKNyvwL2O-yL-tc0C0bpj3ujXgNL3Qiq25AOy1pR2VB-Mfj2-0_s5qqDYm07fgB3YMvU4Da-L5UXF8caZsqvY7UW84_F0A6URbpOBTZQ3_2fTAlAI5BVIkDA0MVDFGKaSbMVUVmHXvnRYJ-gLaopgYhdCxin8uPQXBWMz8VdUGx26YfQG-n--D)

## Пример структуры ЗАВИСИМОСТИ masscan_jobs
![image](https://mermaid.ink/svg/pako:eNqNUstKMzEUfpWQtaP7oXTlUje6bGRIk7TN_89kSpIRRYSqiIqC4kYX_wV9gipW6q2-wskbeaYdLy0IDoScy3e-853J2aIil4rGNIoiZrz2qYoJ_A89GMIzDEKPhJ2wG3phDx7x3sNEn8AFnME5XMIVnj_wD-9ruCQZd05wk_zKm46ZMWHb8m6HLK0wQ_DTJnGiozLeqOk6XIV9GGAT5LtFcwR34RTvYdjHnuiO4CYcIuIFpTzUFnSdMVNr1uEvDAi8opCyeJJ-htF8baFZIlBbH26RZWcSL-XDkCAMq-6Q9Anu4SUcI0Xp4EQH0MfJTsraTx1H0zqO3nXMk7XJLHnhvxkGW_90HC5EXhifaBkT5y1GdDex3LTVu9_NrZ-OqA2RFlIls0jX4Va6sVdJdMp7bdquFLha2VXjD4Rd10I1vr5clZK5-K1sS6eqsfhhVrlp-iiqVzwzrzybqBwMf_48ZugczZTNuJa4hVslklGPKcVojKZULV6knlFmthHKC5-vbhpBY28LNUeLruReLWqOm5bRuMVTh1Eltc_t8mSzxwu-_QYTqGBi)

## Пример структуры Сервиса masscan
![image](https://mermaid.ink/svg/pako:eNqdUs1KAzEQfpWQc_cFcvDkUS96bKSEZLYN7iZLMitKEaqlIHjwEcQ3ULFQRfsMyRs5bdefFk_mkm---Zj5Jpkx194AF7woCunQYgWCpfs8SYv0nuZ5wvJVvs6TPE1vdE8J3bL0sMqk57TIV-mR1SpGrZx06xrDoJoROziSjtGxbhD1CGrVTw95luZU9JGlZ4LL9JLv6F7kGfWgcJme8g0pPqj1q5ROae1bhwNrBIsYiLHNICg3hK-48QG3GTjXVWtgsKuMIxVMXEcnG2O-xb-c0UT_9fbblbAOu0YREK0bxv5xB0gSFIIoK69wVcuoBiEI5hu03qnql8sI4cxq6HdP3LHG61MIpa2gv_8Nu9x206LY60rsfMduoguI_nkY6XiP1xBqZQ1tyHillBwpBZILggZK1VYouXSXJFUt-uMLp7nA0EKPt42hOfetopWouShVFYkFY9GHw83WrZfv8hO0TRV7)

## Пример структуры Сервиса nmap
![image](https://mermaid.ink/svg/pako:eNqtUsFqAjEQ_ZWQs_sDOfTksb3U46ZI2Iwaupss2dlCEUErQqGHfkLxD6xUsKX1G5I_6riulkqPzWXezJuZF2ZmzDOngQueJIm0aDAHwcJLnIZt-AybOGVxFh_iNM7DB9k5oScWlnsmrMM2zsKK2UKV0jYNhl6VI3Z5LS2jZ2y_ykZQqDQs4yJsqOOKhTXBXXiLz2S3cUEC5O7Ca3ykjC_SfZfSqixztcW-0YJV6CliyiMqnUdBzfHmIONq_EuHPvdvSoQr8Hcmg75VBTR8q14BorHDKu21gHJPVFOS7gfUhrTLbsEPTA5p9wRb7ne_JLlo68-GeU60DoV_BiEt7_ACfKGMpuWO95mSI1EguSCoYaDqHCWXdkKpqkbXu7cZF-hr6PC61AqhaxQttOBioPKKoqANOn91OJjmbibfor35-w)

---

# Очереди

## system_events (pub/sub)

Через эту очередь проходят сообщения для конфигурации "сервисов" во время их исполнения. Например, подписка и отписка от очередей или обновление базы известных паролей.